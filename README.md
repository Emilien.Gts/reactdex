<!-- NAME -->

## Name

ReactDex

<!-- DESCRIPTION -->

## Description

This project takes up the principle of Pokedex in a different way than a simple pokemon list. This project was carried out using the [ReactJS Framework](https://fr.reactjs.org/). This project also used the : [PokeAPI](https://pokeapi.co/).
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Context

This project was carried out in a personal setting. Following a training on React, I wanted to have a concrete project, as usual, to be able to develop my skills and solidify my bases.

## Installation

To begin, you need to install the project's dependencies using the following command:

    yarn install

To do this, you need to install [yarn](https://yarnpkg.com/) (or you can use [npm](https://www.npmjs.com/))

## Usage

You can start the application using the following command:

    yarn start

Or its npm equivalent.

## Rendering of the project

![Rendering of ReactDex](https://gitlab.com/Emilien.Gts/reactdex/-/raw/master/src/images/reactdex.png?raw=true)

## Credits

The [design](https://dribbble.com/shots/6175056-Pok-dex) is based on the work of : [Mauro E. Wernly](https://dribbble.com/mauro-wernly)
